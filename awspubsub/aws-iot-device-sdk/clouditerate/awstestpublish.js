/*
 * Copyright 2010-2015 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 *  http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

//node.js deps
var sensors = require('./sensors_pi.js');

//npm deps


//app deps
const deviceModule = require('..').device;
const cmdLineProcess = require('./lib/cmdline');

const POLLING_FREQ = 7000;      // Default is 7 seconds whilst testing

const BH1750 = 0x23;
const D6FPH = 0x6C;

//begin module

function processTest(args) {
   //
   // The device module exports an MQTT instance, which will attempt
   // to connect to the AWS IoT endpoint configured in the arguments.
   // Once connected, it will emit events which our application can
   // handle.
   //
   const device = deviceModule({
      keyPath: args.privateKey,
      certPath: args.clientCert,
      caPath: args.caCert,
      clientId: args.clientId,
      region: args.region,
      baseReconnectTimeMs: args.baseReconnectTimeMs,
      keepalive: args.keepAlive,
      protocol: args.Protocol,
      port: args.Port,
      host: args.Host,
      debug: args.Debug
   });

	// get cpuid and use it as a unique identifier
	var cpuInfo = require('fs').readFileSync('/proc/cpuinfo', "utf8");
	var serialNumberPos=cpuInfo.search('Serial') + 10;
	var serialNumber = cpuInfo.substr(serialNumberPos,16);

//debug	console.log('serial number: '+serialNumber);

	// check we have a internet connection - try direct Ethernet first, then WiFi
	var os = require('os');
	var ipAddress = null;
	var netUpInterval;
	
	if (!ipAddress) {
	    netUpInterval = setInterval(function(){
    		console.log('Waiting for Ethernet');
    		if(os.networkInterfaces().eth0 && os.networkInterfaces().eth0.length > 0 && os.networkInterfaces().eth0[0].address) {
        		ipAddress = os.networkInterfaces().eth0[0].address;
        		console.log('Ethernet connected: ip: '+ipAddress);
        		clearInterval(netUpInterval);
		    } else {
		        console.log('Waiting for WiFi');
    		    if(os.networkInterfaces().wlan0 && os.networkInterfaces().wlan0.length > 0 && os.networkInterfaces().wlan0[0].address) {
        		    ipAddress = os.networkInterfaces().wlan0[0].address;
        		    console.log('WiFi connected: ip: '+ipAddress);
        		    clearInterval(netUpInterval);
    		    }
		    }
	    },500);
	}
	
    device
	.on('connect', function() {
        	console.log('connected as '+serialNumber+' with '+ipAddress);
//todo        	device.register(serialNumber);
        	    	
        	setInterval(function() {

        	       var sensorIds = sensors.get1WireIds();
        	       
//                 Debug - enable line below to ignore 1Wire        	       
//        	       sensorIds = [];

        	       var I2C = sensors.initI2C();
        	       // console.log('I2C addr: '+I2C[0].toString(16));
        	       
        	       if (I2C) {
        	            I2C.forEach(function (sensorType) {
        	                if (sensorType === BH1750) { sensorIds.push('bh1750') }
        	                if (sensorType === D6FPH) { sensorIds.push('d6fph') }
        	            });
        	       }

        	       var dhtInit = sensors.initDHT22();
        	       if (dhtInit && I2C) { sensorIds.push('dht22'); }   // dht initialize always returns true, so check we have the board connected by looking for I2C ...
        	       
        	       if (sensorIds.length < 1) {
        	            console.log('Waiting for sensors ...');
        	       } else {
          	            console.log('Found '+sensorIds.length + ' sensors ...');
        	       }

        	       sensorIds.forEach(function (id) {
            		    var data = sensors.getSensorData(id);
            		    device.publish('sensordata/'+serialNumber+'\/'+id, JSON.stringify(data));
        	       });
        	    console.log('---------------------------------------------------------');
        	}, POLLING_FREQ);
    	});


   device
      .on('close', function() {
         console.log('close');
      });
   device
      .on('reconnect', function() {
         console.log('reconnect');
      });
   device
      .on('offline', function() {
         console.log('offline');
      });
   device
      .on('error', function(error) {
         console.log('error', error);
      });
   device
      .on('message', function(topic, payload) {
         console.log('message', topic, payload.toString());
      });

}

module.exports = cmdLineProcess;

if (require.main === module) {
   cmdLineProcess('connect to the AWS IoT service and publish to topic using MQTT',
      process.argv.slice(2), processTest);
}
