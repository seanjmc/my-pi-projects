// methods to access the sensor data
// Dallas Semiconductors DS18B20 temperature sensors, 1 Wire protocol
var ds18b20 = require('ds18b20');

// DHT22 sensor, proprietary protocol
var dht22 = require('node-dht-sensor');

// i2c-bus, used for BH1750 lux sensor and Omron D6F-PH pressure sensor
var bus = require('i2c-bus');

const DHT22 = 22;                           // DHT sensor type is DHT22
const DHT_GPIO = 6;                         // DHT GPIO pin is 6

const BH1750 = 0x23;                        // Lux sensor i2c slave address
const ONE_TIME_HIGH_RES_MODE_1 = 0x20;      // BH1750 read option

const D6FPH = 0x6C;                         // Diff Pressure sensor i2c slave address


var tempSensorId = [];


exports.initI2C = function () {
    busI2C = bus.openSync(1);
    if (busI2C) {
        I2Caddr = busI2C.scanSync();
        I2Caddr.forEach( function(i) {
            // console.log('I2C addr: '+i.toString(16));
        });
        if (I2Caddr.length > 0) {
            return I2Caddr;
        }
    }
}


exports.initDHT22 = function() {
     dhtinit = dht22.initialize(DHT22,DHT_GPIO);  // DHT22 on GPIO 6  
     // console.log('dht init: '+dhtinit);
     return dhtinit;  // DHT22 on GPIO 6
}

exports.get1WireIds = function() {
    ds18b20.sensors(function(err, id) {
        if (err) {
            console.log('Error: '+err);
        } else {
            // ignore spurious sensors
            if (id.toString().substring(0,3) != '00-') {
                tempSensorId = id;
            }
        }
    });
    return tempSensorId;
    
}

exports.getSensorData = function(id) {
    var data = {};

        
// debug    console.log('thisId : '+id);
    switch (id)
    {
    case 'dht22':

        var readdht = dht22.read();
        data.dhttemp = readdht.temperature.toFixed(2);
        data.dhthumidity = readdht.humidity.toFixed(2);
        console.log('id: '+id+' DHTtemp: '+ data.dhttemp + ' DHThumidity: '+data.dhthumidity);
        break;
        
    case 'bh1750':
        
        var buff = new Buffer(2);
        // Debug console.log('buff: '+buff);
        readthis = busI2C.readI2cBlockSync(BH1750,ONE_TIME_HIGH_RES_MODE_1,buff.length,buff);
        data.lux = convertToNumber(buff).toFixed(2);
        console.log('id: '+id+ ' lux: '+data.lux);
        break;
        
    case 'd6fph':

        d6fph = getD6fphReadings(busI2C);
        data.d6ftemp = d6fph[0].toFixed(2);
        data.d6fpressure = d6fph[1].toFixed(2);
        console.log('id: '+id+' D6Ftemp: '+ data.d6ftemp + ' D6Fpressure: '+data.d6fpressure);        
        break;
        
    default:        // must be a 1Wire temp sensor
    
        data.temp = ds18b20.temperatureSync(id).toFixed(2);
        console.log('id: '+id+ ' temp: '+data.temp);

    }

    // data.filterDP = (1 + random(-1,1)).toFixed(2);  // faked for now
 
    data.readingTime = new Date().toISOString();
    return data;
}

function getD6fphReadings(i2cbus) {
    var d6fdata = [];
    var rawTemp = [];
    var rawPress = [];
    
    const INITIALIZE = new Buffer([0x0b, 0x00]);
    const MCU = new Buffer([0x00, 0xd0, 0x40, 0x18, 0x06]);
    const PRESSURE_WRITE = new Buffer([0x00, 0xd0, 0x51, 0x2c]);
    const TEMPERATURE_WRITE = new Buffer([0x00, 0xd0, 0x61, 0x2c]);
    const EXTRA_WRITE = new Buffer([0x07]);

    busI2C.i2cWriteSync(D6FPH, 2, INITIALIZE);
    
    // First read temperature
    busI2C.i2cWriteSync(D6FPH, 5, MCU);

    setInterval( function() {
    },33);                   // need to wait 33 ms whilst MCU is working
    
    busI2C.i2cWriteSync(D6FPH, 4, TEMPERATURE_WRITE);
    busI2C.i2cWriteSync(D6FPH, 1, EXTRA_WRITE);
    
    rawTemp.push(busI2C.receiveByteSync(D6FPH));
    rawTemp.push(busI2C.receiveByteSync(D6FPH));
    console.log('Temp_rawHi: '+rawTemp[0]+' Temp_rawLo: '+rawTemp[1]);
    
    // Now read pressure
    busI2C.i2cWriteSync(D6FPH, 5, MCU);

    setInterval( function() {
    },33);                   // need to wait 33 ms whilst MCU is working
    
    busI2C.i2cWriteSync(D6FPH, 4, PRESSURE_WRITE);
    busI2C.i2cWriteSync(D6FPH, 1, EXTRA_WRITE);
    
    rawPress.push(busI2C.receiveByteSync(D6FPH));
    rawPress.push(busI2C.receiveByteSync(D6FPH));
    console.log('Press_rawHi: '+rawPress[0]+' Press_rawLo: '+rawPress[1]);

    
    d6fdata.push(convertD6FtempToCelcius(convertToNumber(rawTemp)));
    d6fdata.push(convertD6FpressToPa(convertToNumber(rawPress)));
    
    return d6fdata;
}

function random (low, high) {
    return Math.random() * (high - low + 1) + low;
}

function convertToNumber (data) {
    return ((data[1] + (256 * data[0])) /1.2);
}

function convertD6FtempToCelcius (rawd6ftemp) {
    return ((rawd6ftemp - 10214) / 37.39) + 50;
}


function convertD6FpressToPa (rawd6fpress) {
    return (((rawd6fpress - 1024) * 50 * 2  / 60000) - 50);
}